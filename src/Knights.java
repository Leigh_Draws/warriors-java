
public class Knights extends Warriors {
    String horseName;
    boolean speed;
    int armor;
    Weapons weapon;

    
    public void attack(Warriors opponent) {

        this.force += this.weapon.damage;
        super.attack(opponent);
    }
    public Knights(String warriorName, boolean warriorAlive, int warriorLife, String knightHorseName, int knightArmor, Weapons knightWeapon) {

        super(warriorName, warriorAlive, warriorLife);
        horseName = knightHorseName;
        armor = knightArmor;
        life = warriorLife;
        life += knightArmor;
        speed = true;
        weapon = knightWeapon;
    }
}