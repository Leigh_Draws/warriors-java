public class Warriors {
    // Déclaration des champs - blueprint de ce à quoi vont ressembler les warriors
    String name;
    boolean isAlive;
    int force;
    int life;

    public Warriors(String warriorName, boolean warriorAlive, int warriorLife) {
        System.out.println("Warrior created!");
        name = warriorName;
        isAlive = warriorAlive;
        life = warriorLife;
        force = 50;
    }

    public boolean isItAlive() {
        if (this.life <= 0) {
            return !this.isAlive;
        } else {
            return this.isAlive;
        }
    }

    public void checkLifePoints(){
        boolean itAlive = this.isItAlive();
        if (itAlive) {
            System.out.println(this.name + " est encore en vie.");
            System.out.println("Points de vie de " + this.name + " :" + this.life);
        } else {
            System.out.println(this.name + " est mort ...");
        }
    }

    public void attack(Warriors opponent){
        final int opponentFullLife = opponent.life;
        opponent.life -= this.force ;
        System.out.println(this.name + " attaque " + opponent.name + ".");
        System.out.println(opponent.name + " perd " + this.force + " points de vie.");
        if ( opponentFullLife / 3 >= opponent.life) {
            System.out.println("C'est bientôt la fin pour " + opponent.name);
        } else if (opponentFullLife / 2 >= opponent.life) {
            System.out.println("C'est super efficace !");
        }
    }

    public static void main(String[] args) {
        Warriors titouan = new Warriors("Titouan", true, 200);
        Weapons tacos = new Weapons("Tacos", 20, 60);
        Weapons sword = new Weapons("Epée", 40, 100);
        Knights jamie = new Knights("Jamie", true, 220, "Mirabelle", 50, sword);
        Knights michel = new Knights("Michel", true, 250, "Cascadelle", 20, tacos);
        System.out.println(jamie.force);
        jamie.attack(michel);
        michel.checkLifePoints();
    }

}